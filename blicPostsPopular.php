<?php
/* Template Name: Blic Posts Popular */

use GfWpPluginContainer\Wp\PostHelper;
function getUtmParamsForPopular() {
    $params = '';
    $utmSource = '';
    $utmMedium = '';
    if (isset($_GET['utm_source']) && $_GET['utm_source'] !== ''){
        $utmSource = $_GET['utm_source'];
    }
    if (isset($_GET['utm_medium']) && $_GET['utm_medium'] !== ''){
        $utmMedium = $_GET['utm_medium'];
    }
    if ($utmSource !== '' && $utmMedium === '') {
        $params = '?utm_source='.$utmSource;
    }
    if ($utmMedium !== '' && $utmSource === '') {
        $params = '?utm_medium='.$utmMedium;
    }
    if ($utmMedium !== '' && $utmSource !== ''){
        $params = '?utm_source='.$utmSource.'&utm_medium='.$utmMedium;
    }
    return $params;
}

function getSliderSizeFromParam() {
    if(isset($_GET['size']) && $_GET['size'] !== '') {
        return $_GET['size'];
    }
    return '';
}
function blic_popular_posts() {
	$popularPosts = postHelper::getPopularPosts( 29 );
    $html = '';
	foreach ( $popularPosts as $post ) {
        $href = get_the_permalink($post->ID);
        $title = esc_attr($post->post_title);
        $html .= '<div class="popularItemFromWidget">';
        $html .= '<article>';
        $html .= '<a target="_blank" 
        title="' . $title . '"
        class="popularImageFromWidget" 
        href="' .$href . getUtmParamsForPopular() . '">
        <img src="' . get_the_post_thumbnail_url( $post->ID, 'related' ) .'" alt="' . $title . '">
        </a>';
        $html .= '<div>';
        $html .= '<h2 class="popularTitleFromWidget">
        <a target="_blank" href="' . $href . getUtmParamsForPopular() .'" title="' . $title . '">' . $title .'</a>
        </h2>';
        $html .= '</div>';
        $html .= '</article>';
        $html .= '</div>';
	}

	return $html;
}

?>

<!DOCTYPE html>
<html>

<head>
    <title>Srpskainfo slider widget</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/all.css" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css"/>
</head>
<body>
<div class="popularWidgetContainer <?=getSliderSizeFromParam()?>">
    <div class="popularWidgetContainerHeader">
        <h3 class="popularWidgetTitle">
            <a href="https://srpskainfo.com<?=getUtmParamsForPopular()?>" target="_blank" title="<?= get_bloginfo('name') ?>">
                Pogledajte još ...
            </a>
        </h3>
    </div>
    <div class="popular-news owl-carousel">
        <?php echo blic_popular_posts(); ?>
    </div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.3/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
<script>
    $(document).ready(function () {
        let items = 2;
        let container = document.querySelector('.popularWidgetContainer ');
        if(container && container.classList.contains('extraLarge')) {
            items = 4;
        }
        var owlRef = $('.popular-news');
        $(".popular-news").owlCarousel({
            items: items,
            margin: 10,
            dots: false,
            nav: true,
            stagePadding: 10,
            navText: ['<span class="fa fa-chevron-left"></span>', '<span class="fa fa-chevron-right"></span>'],
            onInitialize: function(element) {
                owlRef.children().sort(function() {
                    return Math.round(Math.random()) - 0.5;
                }).each(function() {
                    $(this).appendTo(owlRef);
                });
            },
        });
    });
</script>
</body>
<style>

    *{
        box-sizing: border-box;
        margin: 0;
    }
    .popularWidgetContainer{
        width: 300px;
        height: 250px;
        box-shadow: 0 1px 1px rgba(173,168,168,0.1);
        border: 1px solid rgba(0,0,0,0.1);
        border-top: none;
        background-color:#fff;
    }
    .popularWidgetContainer.medium{
        width: 336px;
        height: 280px;
    }
    .popularWidgetContainer.large{
        width: 360px;
        height: 250px;
    }
    .popularWidgetContainer.extraLarge {
        width:580px;
        height:280px;
    }
    .popularWidgetContainerHeader{
        background: #da2c2c;
        margin:0 -1px  10px;
    }

    .popularWidgetContainerHeader .popularWidgetTitle a{
        margin: 0 !important;
        font-size: 16px;
        line-height: 20px;
        padding: 5px 10px;
        font-family: 'Roboto', sans-serif;
        color:#fff;
        font-weight: 700;
        text-decoration: none;
        display: block;
    }
    .popularItemFromWidget .popularImageFromWidget{
        position: relative;
        margin-bottom: 10px;
        display: block;

    }
    .popularItemFromWidget .popularImageFromWidget:after{
        padding-top: 100px;
        content: '';
        display: block;
        background: #ccc;
    }
    .popularWidgetContainer.v2 .popularItemFromWidget .popularImageFromWidget:after{
        padding-top: 125px;
    }
    .popularWidgetContainer.v3 .popularItemFromWidget .popularImageFromWidget:after{
        padding-top: 100px;
    }
    .popularItemFromWidget .popularImageFromWidget img{
        position: absolute;
        top:0;
        left: 0;
        width: 100%;
        height: 100%;
        object-fit: cover;

    }
    .popularTitleFromWidget a{
        display: block;
        color:#212121;
        font-size: 12px;
        line-height: 18px;
        display: -webkit-box;
        -webkit-line-clamp: 5;
        -webkit-box-orient: vertical;
        overflow: hidden;
        text-decoration: none;
        font-family: 'Roboto', sans-serif;
        font-weight: 700;

    }
    .popularWidgetContainer .owl-nav{
        position: absolute;
        top: 50%;
        left: 0;
        width: 100%;
    }
    .popularWidgetContainer .owl-nav .disabled{
        display: none;
    }
    .popularWidgetContainer .owl-nav .owl-prev,
    .popularWidgetContainer .owl-nav .owl-next
    {
        width: 32px;
        height: 38px;
        line-height: 38px;
        font-size: 12px;
        background: rgba(17,17,17,1) !important;
        transform: translateY(-50%);
        position: absolute;
        color:#fff;
        text-align: center;
    }
    .popularWidgetContainer .owl-nav button span{
        line-height: 38px;
        color:#fff;
        text-align: center;
        font-size: 14px;
    }
    .popularWidgetContainer .owl-nav .owl-prev{
        left: 10px;
    }
    .popularWidgetContainer .owl-nav .owl-next{
        right: 10px;
    }

</style>
</html>