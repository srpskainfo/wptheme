<?php

if (isset($_GET['q'])) {
	$params = explode('/', $_GET['q']);
} else {
    $params = explode('/', $_SERVER['REQUEST_URI']);
}
$lastParam = count($params) - 1;
if (!is_numeric($params[$lastParam])) {
    maybeRedirectToPost($params[$lastParam]);
}
if ($params[$lastParam] === 'false' && strlen($params[$lastParam-1]) > 10) {
    maybeRedirectToPost($params[$lastParam-1]);
}

function maybeRedirectToPost($slug) {
    $post = get_post(url_to_postid($slug));
    if ($post) {
        wp_safe_redirect(home_url($slug));
        exit();
    }
}

get_header();

$dotMetricsId = 4595;
//Check for mobile & AMP
echo '<div class="container">';
	echo '<p> Ups, izgleda da ste naišli na stranicu koja ne postoji.</p>';
	echo '<a href="'.get_home_url().'" title="homepage">Vrati se na početnu</a>';
echo '</div>';

get_footer(); ?>