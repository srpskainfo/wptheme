<?php
/* Template Name: Iframe Blic New */

function printBlicIframePosts() {
    $page = get_post(get_option('page_on_front'));
    $block_name = 'wpplugincontainer/gfnewsblock';
    $blocks = array_filter( parse_blocks( $page->post_content ), function( $block ) use( $block_name ) {
        return $block_name === $block['blockName'];
    });

    $posts = get_posts([
        'include' => [
            $blocks[4]['innerBlocks'][0]['innerBlocks'][0]['innerBlocks'][0]['innerBlocks'][0]['attrs']['post1'],
            $blocks[4]['innerBlocks'][0]['innerBlocks'][0]['innerBlocks'][0]['innerBlocks'][0]['attrs']['post2'],
            $blocks[4]['innerBlocks'][0]['innerBlocks'][0]['innerBlocks'][0]['innerBlocks'][0]['attrs']['post3'],
            $blocks[4]['innerBlocks'][0]['innerBlocks'][0]['innerBlocks'][0]['innerBlocks'][0]['attrs']['post4'],
            $blocks[4]['innerBlocks'][0]['innerBlocks'][0]['innerBlocks'][0]['innerBlocks'][0]['attrs']['post5']
        ]
    ]);
    $params = '';
    $utmSource = '';
    $utmMedium = '';
    if (isset($_GET['utm_source']) && $_GET['utm_source'] !== ''){
        $utmSource = $_GET['utm_source'];
    }
    if (isset($_GET['utm_medium']) && $_GET['utm_medium'] !== ''){
        $utmMedium = $_GET['utm_medium'];
    }
    if ($utmSource !== '' && $utmMedium === '') {
        $params = '?utm_source='.$utmSource;
    }
    if ($utmMedium !== '' && $utmSource === '') {
        $params = '?utm_medium='.$utmMedium;
    }
    if ($utmMedium !== '' && $utmSource !== ''){
        $params = '?utm_source='.$utmSource.'&utm_medium='.$utmMedium;
    }

    $html = '';
    foreach ($posts as $post) {
        $href = get_permalink($post->ID) . $params;
        $imageUrl = esc_url(wp_get_attachment_image_url(get_post_thumbnail_id($post->ID), 'medium'));
        $imageWidth = 120;
        $imageHeight = 82;
        $imageAlt = esc_attr($post->post_title);
        $html .= <<<ARTICLE
        <article class="news most-read-event">
            <div class="news__img">
                <a href="$href">
                <figure>
                    <picture>
                        <source type="image/webp" data-srcset="$imageUrl">
                        <img width="$imageWidth" height="$imageHeight"
                             src="$imageUrl"
                             data-src=$imageUrl"
                             class="lazyload"
                             alt="$imageAlt"
                        />
                    </picture>
                </figure>
            </a>
            </div>
            <div class="news__content">
                <h2><a href="$href">$post->post_title</a></h2>
            </div>
        </article>
        ARTICLE;
    }

    return $html;
}

$text = '<cdf:block name="block-detail" content-type="text/html" cache="min">
            <section class="news-box news-box--color news-box--color-si">
                <section class="news-box__header news-box__header--logo">
                    <h1><a href=""><?xml version="1.0" encoding="UTF-8"?>             <svg enable-background="new 0 0 350 64" version="1.1" viewBox="0 0 350 64" xml:space="preserve" xmlns="http://www.w3.org/2000/svg">                             <style type="text/css">                                 .st0{fill:#FFFFFF;}                                 .st1{fill:#EC1C24;}                                 .st2{fill:#3D3D3D;stroke:#3D3D3D;stroke-width:3;stroke-miterlimit:10;}                                 .st3{fill:#FFFFFF;stroke:#FFFFFF;stroke-width:3;stroke-miterlimit:10;}                                 .st4{fill:#3D3D3D;}                                 .st5{fill:#EA1B24;}                             </style>                 <rect class="st0" x="126.3" y="5.5" width="194.8" height="52.8"/>                 <path class="st4" d="m234.6 10.5c0.9 0 1.6 0.3 2.2 0.9s0.9 1.3 0.9 2.2-0.3 1.6-0.9 2.2-1.4 0.9-2.2 0.9c-0.9 0-1.6-0.3-2.2-0.9s-0.9-1.4-0.9-2.2c0-0.9 0.3-1.6 0.9-2.2s1.4-0.9 2.2-0.9z"/>                 <rect class="st4" x="232.8" y="20.2" width="3.8" height="29.4"/>                 <path class="st4" d="m241.6 38.4s0-6 0.1-7c0.4-3.4 1.5-6.1 2.8-7.6 1.3-1.4 4.9-4.3 10.5-4.3 6.2 0 10.2 4.3 11 6.2s1.2 4.8 1.2 8.8v15.1h-3.7v-16c0-3.4-0.1-3.8-0.4-4.9-0.4-1.9-1.3-3.3-2.6-4.1s-2.9-1.5-6-1.4c-2.3 0.1-4.2 0.6-6 2.2-2.3 2.1-3 4.4-3 8.3v15.9h-3.8v-11.2z"/>                 <path class="st4" d="m283.2 12.9c-1.9-1-3.7-1.1-5.5-0.2-1.6 0.9-2.6 3.2-3.1 7.1v2.9h7v3.2h-7v23.7h-3.8v-30.5c0.1-2.4 0.5-4.4 1.5-5.9 1.4-3 3.7-4.5 6.9-4.6 1.6-0.4 3.4 0 5.2 1.2-0.3 0.7-0.7 1.7-1.2 3.1z"/>                 <path class="st4" d="m301.5 18.7c4.5 0 8.3 1.6 11.2 4.9 2.7 3 4 6.5 4 10.6s-1.4 7.7-4.3 10.7c-2.9 3.1-6.5 4.6-11 4.6s-8.2-1.5-11-4.6-4.3-6.6-4.3-10.7 1.3-7.6 4-10.6c3.1-3.2 6.9-4.9 11.4-4.9zm0 3.7c-3.1 0-5.8 1.2-8.1 3.5-2.7 2.8-3.8 6.4-3.2 10.7 0.1 0.4 0.1 0.7 0.2 1.1 1 3.2 2.8 5.5 5.3 6.9 1.7 1 3.7 1.5 5.7 1.5 2.1 0 4-0.5 5.8-1.5 1.7-1 3.1-2.4 4.1-4.3 1-1.8 1.5-3.8 1.5-6 0-3.3-1.1-6.1-3.4-8.4-2.1-2.3-4.8-3.5-7.9-3.5z"/>                 <rect class="st5" x="28.9" y="5.5" width="194.8" height="52.8"/>                 <path class="st0" d="m53.7 17.8-2.8 3c-2.4-2.3-4.7-3.5-7-3.5-1.4 0-2.7 0.5-3.7 1.4s-1.5 2.1-1.5 3.4c0 1.1 0.4 2.1 1.3 3.1 0.8 1.1 2.6 2.3 5.3 3.7 3.3 1.7 5.6 3.3 6.7 4.9 1.2 1.6 1.7 3.4 1.7 5.4 0 2.8-1 5.2-3 7.2s-4.5 2.9-7.4 2.9c-2 0-3.9-0.4-5.7-1.3-1.8-0.8-3.3-2-4.5-3.6l2.8-3.2c2.3 2.6 4.7 3.8 7.2 3.8 1.8 0 3.3-0.6 4.5-1.7s1.9-2.5 1.9-4c0-1.3-0.4-2.4-1.2-3.4s-2.7-2.2-5.6-3.7c-3.1-1.6-5.2-3.2-6.4-4.8-1.1-1.6-1.7-3.3-1.7-5.4 0-2.6 0.9-4.8 2.7-6.5s4-2.6 6.8-2.6c3.2 0.2 6.4 1.8 9.6 4.9z"/>                 <path class="st0" d="m60.9 29.4c0-2.4 1.2-9 6.4-12.7 4.1-2.9 9.7-2.4 11-1.6l-2.2 3.6c-6.7-0.8-9.5 4.9-10.2 7.9-0.4 1.8-0.5 4.1-0.5 4.1v18.6h-4.5v-19.9z"/>                 <path class="st0" d="m90.7 14.9c2.5-1.1 5.1-1.6 7.9-1.6 4.9 0 9.1 1.8 12.6 5.3s5.2 7.8 5.2 12.8c0 5.1-1.7 9.4-5.2 12.9s-7.6 5.3-12.5 5.3c-2.8 0-5.3-0.6-7.6-1.7s-4.4-2.8-6.3-5.1v15.8h-4.4v-29.2s1.7-6.9 3.6-9.3c1.7-2.1 4.2-4.1 6.7-5.2zm-2.2 6.6c-2.6 2.7-4 6-4 10 0 2.6 0.6 5 1.8 7.1s2.9 3.8 5 5c2.2 1.2 4.5 1.8 6.9 1.8s4.6-0.6 6.8-1.8c2.1-1.2 3.8-3 5-5.2s1.9-4.6 1.9-7c0-2.5-0.6-4.8-1.9-7-1.2-2.2-2.9-3.9-5-5.1s-4.4-1.8-6.9-1.8c-3.7 0-6.9 1.4-9.6 4z"/>                 <path class="st0" d="m142.7 17.8-2.8 3c-2.4-2.3-4.7-3.5-7-3.5-1.4 0-2.7 0.5-3.7 1.4s-1.5 2.1-1.5 3.4c0 1.1 0.4 2.1 1.3 3.1 0.8 1.1 2.6 2.3 5.3 3.7 3.3 1.7 5.6 3.3 6.7 4.9 1.2 1.6 1.7 3.4 1.7 5.4 0 2.8-1 5.2-3 7.2s-4.5 2.9-7.4 2.9c-2 0-3.9-0.4-5.7-1.3-1.8-0.8-3.3-2-4.5-3.6l2.8-3.2c2.3 2.6 4.7 3.8 7.2 3.8 1.8 0 3.3-0.6 4.5-1.7s1.9-2.5 1.9-4c0-1.3-0.4-2.4-1.2-3.4s-2.7-2.2-5.6-3.7c-3.1-1.6-5.2-3.2-6.4-4.8-1.1-1.6-1.7-3.3-1.7-5.4 0-2.6 0.9-4.8 2.7-6.5s4-2.6 6.8-2.6c3.1 0.2 6.4 1.8 9.6 4.9z"/>                 <path class="st0" d="m150.5 5.6h4.5v23.2l16-13.9h6.6l-14.6 13.1 15.6 21.4h-6.3l-13.6-18.6-3.7 3.1v15.5h-4.5v-43.8z"/>                 <path class="st0" d="m199.4 13.1c5.3 0 9.7 1.9 13.2 5.8 3.2 3.5 4.8 7.7 4.8 12.5v18.2h-4.7v-7.8c-2.5 4.6-6.9 7.1-13.4 7.7-5.3 0-9.6-1.8-13-5.4s-5-7.8-5-12.6 1.6-8.9 4.8-12.5c3.6-3.9 8-5.9 13.3-5.9zm0 4.4c-3.7 0-6.8 1.4-9.5 4.1-2.6 2.7-4 6-4 9.9 0 2.5 0.7 4.9 2 7.2 1.4 2.3 3.6 4.3 6.9 5.9 8.1 1.8 13.9-1.9 17.3-11.1 0.2-2.9-0.1-5.8-0.9-8.4-0.6-1.2-1.4-2.4-2.4-3.5-2.6-2.8-5.7-4.1-9.4-4.1z"/>                         </svg>         </a></h1>
                </section>';

$text .= printBlicIframePosts();

$text .= '</section></cdf:block>';

echo $text;