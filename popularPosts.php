<?php
/**
 * Template Name: Popular Posts
 */

use \GfWpPluginContainer\Indexer\Repository\Article as ArticleRepo;

get_header();
$paged = (get_query_var('paged') > 0) ? get_query_var('paged') : 1;
$perPage = PER_PAGE;
$catName = 'Popularno';
$dotMetricsId = getDotMetricsId('popularno');
$ajaxAction = 'popularPosts';
$ajaxTermValue = 'popular';

//$searchFunctions = new \GfWpPluginContainer\Elastic\Functions($wpdb);
if (USE_ELASTIC !== true) {
    $sortedItems = ArticleRepo::getItemsFromWp([
        'numberposts' => PER_PAGE,
        'order' => 'DESC',
        'suppress_filters' => false,
        'meta_key' => 'gfPostViewCount',
        'orderby' => 'meta_value_num',
    ]);
} else {
    $termMock = new \Stdclass();
    $termMock->term_id = 0;
    $sortedItems = ArticleRepo::getItemsFromElasticBy('popular', $termMock, $paged, PER_PAGE);
}
if ($isApp) {
	include "templates/archive/archiveMobileApp.php";
	wp_footer();
} else {
	include "templates/archive/archiveDesktop.php";
	get_footer();
}
