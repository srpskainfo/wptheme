<?php
// Check if request is coming from an APP and set route param in var
global $isApp;
get_header();
dynamic_sidebar('single_post_top_banner');
if ($isApp) {
    include 'templates/single/singleMobileApp.php';
    echo '<script src="https://www.nekretnine.rs/widget-content-exchange-slider.js"></script>';
} else {
    include 'templates/single/singleDesktop.php';
    if(!wp_is_mobile()) {
//        echo '<script src="https://www.nekretnine.rs/widget-content-exchange.js?latest"></script>';
    } else {
        echo '<script src="https://www.nekretnine.rs/widget-content-exchange-slider.js"></script>';
    }
}
get_footer();
