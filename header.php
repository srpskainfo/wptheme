<!DOCTYPE html>
<html lang="sr">
<head>
    <link rel="stylesheet" href="https://cdn.consentmanager.mgr.consensu.org/delivery/cmp.min.css" />
    <?php if(is_single()){ echo '<script>
        if(!onetAds){
            var onetAds = {
                target: "srpska_info/main",
                async: 1,
                tid: "EA-5437314"
            };
        }
        var pulse2EmbedConfig = {
            pulsevideo: {
                player        : \'flowplayer\',
        params        : {
            autoplay: true,
                autoplayNext: false,
                enableAds: false,
                enableGoogleAnalytics: "UA-83076923-7",
                ga: {
                ga_instances: ["UA-83076923-7"],
                    event_actions: {
                    video_start: "video_start",
                        video_complete: "video_complete",
                        ad_start_preroll: "ad_start",
                        ad_start_midroll: "ad_start",
                        ad_start_postroll: "ad_start"
                }
            },
            customAdConfig: {
                adSchedule: [
                    {
                        adTag: \'https://pubads.g.doubleclick.net/gampad/ads?iu=/22022651321/SrpskaInfo_Video&description_url=https%3A%2F%2Fsrpskainfo.com&tfcd=0&npa=0&sz=300x250%7C400x300%7C640x480&gdfp_req=1&output=vast&unviewed_position_start=1&env=vp&impl=s&correlator=\',
                time : 0
            }
            ]
            },
        },
        plugins: [
            {
                script: \'https://imasdk.googleapis.com/js/sdkloader/ima3.js\'
        },
        {
            script: \'https://ocdn.eu/video-apps/flowplayer/v2.7.3/dist/plugins/ads.min.js\'
        },
        {
            script: \'https://ocdn.eu/blic/flowplayer/v1.1.0/bundle.min.js\'
        },
        {
            script: \'https://ocdn.eu/blic/flowplayer/googleanalytics/v2.7.3/googleanalytics.min.js\'
        },
        ]
        }
        };
    </script>';}?>
    <script>window.gdprAppliesGlobally=true;if(!("cmp_id" in window)||window.cmp_id<1){window.cmp_id=0}if(!("cmp_cdid" in window)){window.cmp_cdid="22c7c0670118"}if(!("cmp_params" in window)){window.cmp_params=""}if(!("cmp_host" in window)){window.cmp_host="b.delivery.consentmanager.net"}if(!("cmp_cdn" in window)){window.cmp_cdn="cdn.consentmanager.net"}if(!("cmp_proto" in window)){window.cmp_proto="https:"}window.cmp_getsupportedLangs=function(){var b=["DE","EN","FR","IT","NO","DA","FI","ES","PT","RO","BG","ET","EL","GA","HR","LV","LT","MT","NL","PL","SV","SK","SL","CS","HU","RU","SR","ZH","TR","UK","AR","BS"];if("cmp_customlanguages" in window){for(var a=0;a<window.cmp_customlanguages.length;a++){b.push(window.cmp_customlanguages[a].l.toUpperCase())}}return b};window.cmp_getRTLLangs=function(){return["AR"]};window.cmp_getlang=function(j){if(typeof(j)!="boolean"){j=true}if(j&&typeof(cmp_getlang.usedlang)=="string"&&cmp_getlang.usedlang!==""){return cmp_getlang.usedlang}var g=window.cmp_getsupportedLangs();var c=[];var f=location.hash;var e=location.search;var a="languages" in navigator?navigator.languages:[];if(f.indexOf("cmplang=")!=-1){c.push(f.substr(f.indexOf("cmplang=")+8,2).toUpperCase())}else{if(e.indexOf("cmplang=")!=-1){c.push(e.substr(e.indexOf("cmplang=")+8,2).toUpperCase())}else{if("cmp_setlang" in window&&window.cmp_setlang!=""){c.push(window.cmp_setlang.toUpperCase())}else{if(a.length>0){for(var d=0;d<a.length;d++){c.push(a[d])}}}}}if("language" in navigator){c.push(navigator.language)}if("userLanguage" in navigator){c.push(navigator.userLanguage)}var h="";for(var d=0;d<c.length;d++){var b=c[d].toUpperCase();if(g.indexOf(b)!=-1){h=b;break}if(b.indexOf("-")!=-1){b=b.substr(0,2)}if(g.indexOf(b)!=-1){h=b;break}}if(h==""&&typeof(cmp_getlang.defaultlang)=="string"&&cmp_getlang.defaultlang!==""){return cmp_getlang.defaultlang}else{if(h==""){h="EN"}}h=h.toUpperCase();return h};(function(){var n=document;var p=window;var f="";var b="_en";if("cmp_getlang" in p){f=p.cmp_getlang().toLowerCase();if("cmp_customlanguages" in p){for(var h=0;h<p.cmp_customlanguages.length;h++){if(p.cmp_customlanguages[h].l.toUpperCase()==f.toUpperCase()){f="en";break}}}b="_"+f}function g(e,d){var l="";e+="=";var i=e.length;if(location.hash.indexOf(e)!=-1){l=location.hash.substr(location.hash.indexOf(e)+i,9999)}else{if(location.search.indexOf(e)!=-1){l=location.search.substr(location.search.indexOf(e)+i,9999)}else{return d}}if(l.indexOf("&")!=-1){l=l.substr(0,l.indexOf("&"))}return l}var j=("cmp_proto" in p)?p.cmp_proto:"https:";var o=["cmp_id","cmp_params","cmp_host","cmp_cdn","cmp_proto"];for(var h=0;h<o.length;h++){if(g(o[h],"%%%")!="%%%"){window[o[h]]=g(o[h],"")}}var k=("cmp_ref" in p)?p.cmp_ref:location.href;var q=n.createElement("script");q.setAttribute("data-cmp-ab","1");var c=g("cmpdesign","");var a=g("cmpregulationkey","");q.src=j+"//"+p.cmp_host+"/delivery/cmp.php?"+("cmp_id" in p&&p.cmp_id>0?"id="+p.cmp_id:"")+("cmp_cdid" in p?"cdid="+p.cmp_cdid:"")+"&h="+encodeURIComponent(k)+(c!=""?"&cmpdesign="+encodeURIComponent(c):"")+(a!=""?"&cmpregulationkey="+encodeURIComponent(a):"")+("cmp_params" in p?"&"+p.cmp_params:"")+(n.cookie.length>0?"&__cmpfcc=1":"")+"&l="+f.toLowerCase()+"&o="+(new Date()).getTime();q.type="text/javascript";q.async=true;if(n.currentScript){n.currentScript.parentElement.appendChild(q)}else{if(n.body){n.body.appendChild(q)}else{var m=n.getElementsByTagName("body");if(m.length==0){m=n.getElementsByTagName("div")}if(m.length==0){m=n.getElementsByTagName("span")}if(m.length==0){m=n.getElementsByTagName("ins")}if(m.length==0){m=n.getElementsByTagName("script")}if(m.length==0){m=n.getElementsByTagName("head")}if(m.length>0){m[0].appendChild(q)}}}var q=n.createElement("script");q.src=j+"//"+p.cmp_cdn+"/delivery/js/cmp"+b+".min.js";q.type="text/javascript";q.setAttribute("data-cmp-ab","1");q.async=true;if(n.currentScript){n.currentScript.parentElement.appendChild(q)}else{if(n.body){n.body.appendChild(q)}else{var m=n.getElementsByTagName("body");if(m.length==0){m=n.getElementsByTagName("div")}if(m.length==0){m=n.getElementsByTagName("span")}if(m.length==0){m=n.getElementsByTagName("ins")}if(m.length==0){m=n.getElementsByTagName("script")}if(m.length==0){m=n.getElementsByTagName("head")}if(m.length>0){m[0].appendChild(q)}}}})();window.cmp_addFrame=function(b){if(!window.frames[b]){if(document.body){var a=document.createElement("iframe");a.style.cssText="display:none";a.name=b;document.body.appendChild(a)}else{window.setTimeout(window.cmp_addFrame,10,b)}}};window.cmp_rc=function(h){var b=document.cookie;var f="";var d=0;while(b!=""&&d<100){d++;while(b.substr(0,1)==" "){b=b.substr(1,b.length)}var g=b.substring(0,b.indexOf("="));if(b.indexOf(";")!=-1){var c=b.substring(b.indexOf("=")+1,b.indexOf(";"))}else{var c=b.substr(b.indexOf("=")+1,b.length)}if(h==g){f=c}var e=b.indexOf(";")+1;if(e==0){e=b.length}b=b.substring(e,b.length)}return(f)};window.cmp_stub=function(){var a=arguments;__cmapi.a=__cmapi.a||[];if(!a.length){return __cmapi.a}else{if(a[0]==="ping"){if(a[1]===2){a[2]({gdprApplies:gdprAppliesGlobally,cmpLoaded:false,cmpStatus:"stub",displayStatus:"hidden",apiVersion:"2.0",cmpId:31},true)}else{a[2](false,true)}}else{if(a[0]==="getUSPData"){a[2]({version:1,uspString:window.cmp_rc("")},true)}else{if(a[0]==="getTCData"){__cmapi.a.push([].slice.apply(a))}else{if(a[0]==="addEventListener"||a[0]==="removeEventListener"){__cmapi.a.push([].slice.apply(a))}else{if(a.length==4&&a[3]===false){a[2]({},false)}else{__cmapi.a.push([].slice.apply(a))}}}}}}};window.cmp_msghandler=function(d){var a=typeof d.data==="string";try{var c=a?JSON.parse(d.data):d.data}catch(f){var c=null}if(typeof(c)==="object"&&c!==null&&"__cmpCall" in c){var b=c.__cmpCall;window.__cmp(b.command,b.parameter,function(h,g){var e={__cmpReturn:{returnValue:h,success:g,callId:b.callId}};d.source.postMessage(a?JSON.stringify(e):e,"*")})}if(typeof(c)==="object"&&c!==null&&"__cmapiCall" in c){var b=c.__cmapiCall;window.__cmapi(b.command,b.parameter,function(h,g){var e={__cmapiReturn:{returnValue:h,success:g,callId:b.callId}};d.source.postMessage(a?JSON.stringify(e):e,"*")})}if(typeof(c)==="object"&&c!==null&&"__uspapiCall" in c){var b=c.__uspapiCall;window.__uspapi(b.command,b.version,function(h,g){var e={__uspapiReturn:{returnValue:h,success:g,callId:b.callId}};d.source.postMessage(a?JSON.stringify(e):e,"*")})}if(typeof(c)==="object"&&c!==null&&"__tcfapiCall" in c){var b=c.__tcfapiCall;window.__tcfapi(b.command,b.version,function(h,g){var e={__tcfapiReturn:{returnValue:h,success:g,callId:b.callId}};d.source.postMessage(a?JSON.stringify(e):e,"*")},b.parameter)}};window.cmp_setStub=function(a){if(!(a in window)||(typeof(window[a])!=="function"&&typeof(window[a])!=="object"&&(typeof(window[a])==="undefined"||window[a]!==null))){window[a]=window.cmp_stub;window[a].msgHandler=window.cmp_msghandler;window.addEventListener("message",window.cmp_msghandler,false)}};window.cmp_addFrame("__cmapiLocator");window.cmp_addFrame("__cmpLocator");window.cmp_addFrame("__uspapiLocator");window.cmp_addFrame("__tcfapiLocator");window.cmp_setStub("__cmapi");window.cmp_setStub("__cmp");window.cmp_setStub("__tcfapi");window.cmp_setStub("__uspapi");</script>
    <?php
    global $isApp;

    $homeUrl    = '';
    //Get permalink for the current page
    $objId      = get_queried_object_id();
    $currentUrl = get_permalink( $objId );
    $searchUrl = '/pretraga/';
    ?>
    <meta property="fb:admins" content="boris.lakic" />
    <meta property="fb:admins" content="branko.tomic65" />
    <meta property="fb:app_id" content="206933876746107" />
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <?php if ($isApp):
        $searchUrl = '/app/';
    ?><meta name="MobileOptimized" content="320">
    <meta name="HandheldFriendly" content="True">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, maximum-scale=1.0">
    <?php else :?>
        <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php endif;?>
    <link rel="dns-prefetch" href="//dotmetrics.net" />
    <link rel="dns-prefetch" href="//google-analytics.com" />
    <link rel="dns-prefetch" href="//googletagservices.com" />

    <?php wp_head(); ?>
    <!-- Ads -->
    <script data-ad-client="ca-pub-1140012992993761" async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>

    <script async='async' src='https://securepubads.g.doubleclick.net/tag/js/gpt.js'></script>

    <!-- Google tag/ads -->
    <?php if(!$isApp && !wp_is_mobile()):?>
            <script async src="https://adxbid.info/srpskaInfo_desktop_homepage.js" ></script>
    <?php endif;?>
    <?php if($isApp || wp_is_mobile()):?>
        <script async src="https://adxbid.info/srpskaInfo_mobile_homepage.js" ></script>
    <?php endif;?>

    <meta name="google-site-verification" content="GMR8sFRTs8ryNiP4W9ZDAep5Ko8frVczQ2DLqDZvyEk" />
    <script>
        var gptadslots = [];
        var googletag = googletag || {cmd:[]};
    </script>
    <script>
        <?=getAdsScript($currentUrl)?>
    </script>
    <!-- /Google tag/ads -->
    <link rel="manifest" href="/manifest.json" />
<!--  Before Firebase, onesignal script  -->
<!--        <script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async=""></script>-->
<!--        <script>-->
<!--            var OneSignal = window.OneSignal || [];-->
<!--            OneSignal.push(function() {-->
<!--                OneSignal.init({-->
<!--                    appId: "b41623da-e2f2-4bb7-ad9b-3be7f12254c8",-->
<!--                    autoRegister: true,-->
<!--                    notifyButton: {-->
<!--                        enable: false,-->
<!--                    },-->
<!--                });-->
<!--            });-->
<!--    </script>-->
<!-- News widgets -->
    <?php if(!$isApp && (is_single() ||
            (
                    is_page_template('customCategory.php') ||
                    is_page_template('allPosts.php') ||
                    is_page_template('popularPosts.php')
            )
        )):?>
    <link rel="dns-prefetch" href="https://c.aklamator.com" />
    <script>var aklawidgets = ['PSrpRSR','PSrpNRS'];</script>
    <script async src="https://s.aklamator.com/i/w.js"></script>
    <?php endif;?>
    <!-- Facebook Pixel Code -->
    <script>
        !function(f,b,e,v,n,t,s)
        {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};
            if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
            n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t,s)}(window, document,'script',
            'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '344652463724127');
        fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
                   src="https://www.facebook.com/tr?id=344652463724127&ev=PageView&noscript=1"
        /></noscript>

<!-- For test environment    -->
<!--    <script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async=""></script>-->
<!--    <script>-->
<!--        window.OneSignal = window.OneSignal || [];-->
<!--        OneSignal.push(function() {-->
<!--            OneSignal.init({-->
<!--                appId: "439804d0-814c-4235-99d2-c928c49307bc",-->
<!--                notifyButton: {-->
<!--                    enable: true,-->
<!--                }-->
<!--            });-->
<!--        });-->
<!--    </script>-->
    <!-- When it is web and not app    -->
    <?php if(!$isApp): ?>
    <script>
        if ('serviceWorker' in navigator) {
            navigator.serviceWorker.getRegistrations().then(function (registrations) {
                for (let registration of registrations) {
                    if(registration.active.scriptURL === 'https://srpskainfo.com/OneSignalSDKUpdaterWorker.js?appId=b41623da-e2f2-4bb7-ad9b-3be7f12254c8') {
                        registration.unregister()
                    }
                }
            }).catch(function (err) {
                console.log('Service Worker registration failed: ', err);
            });
        }
    </script>
    <?php else :?>
        <script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async=""></script>
        <script>
            var OneSignal = window.OneSignal || [];
            OneSignal.push(function() {
                OneSignal.init({
                    appId: "b41623da-e2f2-4bb7-ad9b-3be7f12254c8",
                    autoRegister: true,
                    notifyButton: {
                        enable: false,
                    },
                });
            });
        </script>
    <?php endif; ?>
</head>
<!-- End Facebook Pixel Code -->
<body <?php body_class(); ?>>
<?php $isMobile = (int) wp_is_mobile();?>
<script>
    let isMobile = "<?=$isMobile?>";
    let isApp = "<?=$isApp?>";
</script>
<?php if(!$isApp):?>
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NG549F8"
                      height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
<?php endif;?>
<?php if($isApp):?>
    <div class="loadingWrapper">

        <div class="loading">

        </div>
    </div>
<?php endif;?>
<div class="overlay"></div>
<aside class="aside-menu">
    <div class="aside-menu__top">
        <span class="aside-menu__close"><i class="fas fa-times"></i></span>
        <form method="get" class="aside-menu__search" action="<?=$searchUrl?>">
            <?php if ($isApp):?>
            <input  class="searchInput" type="search" name="url" value="" placeholder="Pretraga" aria-label="search" />
            <input type="hidden" name="type" value="search"/>
            <?php else: ?>
            <input class="searchInput" type="search" name="query" value="" placeholder="Pretraga" aria-label="search" />
            <?php endif?>
            <button class="buttonSubmitSearch" type="submit" value="search" aria-label="search"><i class="fas fa-search"></i></button>
        </form>
    </div>
    <div class="aside-menu__scroll">
        <nav>
            <ul>
                <?php
                global $cache;
                $key = 'navigation-mobile#Main Navigation Mobile';
                if ($isApp){
	                $key = 'navigation-mobile#Main Navigation Mobile App';
                }
                $html = $cache->get($key);
                if ($html === false) {
                    $menu = wp_get_nav_menu_items('Main Navigation Mobile');
                    /** @var WP_Post $menuItem */
                    $html = '';
                    foreach ($menu as $menuItem) {
                        $menuItemUrl = $menuItem->url;
                        if ($isApp) {
                            if ($menuItem->title === 'Početna') {
                                $menuItemUrl = parseAppUrl('page', '/pocetna');
                            } else {
                                $menuItemUrl = parseAppUrl('page', $menuItem->url);
                            }
                        }
                        if($menuItem->title === 'Sport') {
                            $html .= '<li class="categoryMenuLink"><a title="' . esc_attr($menuItem->title) . '" href="' . $menuItemUrl . '">' . $menuItem->title . '</a></li>';
                        }
                        elseif ($menuItem->title !== 'Digitalna Srpska') {
                            $html .= '<li><a title="' . esc_attr($menuItem->title) . '" href="' . $menuItemUrl . '">' . $menuItem->title . '</a></li>';
                        }
                        else {
                            $html .= '<li class="sponsor">
                                        <a title="Digitalna Srpska" href="' . $menuItemUrl . '">Digitalna Srpska
                                          
                                        </a>
                                        <a href="https://mtel.ba/" title="Mtel" target="__blank">
                                            <div class="sponsor__logo"><span>By</span> <img src="' . CHILD_THEME_DIR_URI . '/images/logo-mtel.svg' . '" alt="By Mtel" width="40" height="40"/></div>
                                            </a>
                                    </li>';
                        }
                    }
                    $cache->set($key, $html, 300);
                }
                echo $html;
                ?>
            </ul>
        </nav>
    </div>
</aside>

<?php
if ($isApp) {
    include("templates/header/view/headerMobile.php");
} else {
    include("templates/header/view/header.php");
}
get_sidebar();
?>
