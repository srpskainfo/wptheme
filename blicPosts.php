<?php
/* Template Name: Blic Posts */

function printBlicPosts() {
    $page = get_post(get_option('page_on_front'));
    $block_name = 'wpplugincontainer/gfnewsblock';
    $blocks = array_filter( parse_blocks( $page->post_content ), function( $block ) use( $block_name ) {
        return $block_name === $block['blockName'];
    });

    $posts = get_posts([
        'include' => [
            $blocks[4]['innerBlocks'][0]['innerBlocks'][0]['innerBlocks'][0]['innerBlocks'][0]['attrs']['post1'],
            $blocks[4]['innerBlocks'][0]['innerBlocks'][0]['innerBlocks'][0]['innerBlocks'][0]['attrs']['post2'],
            $blocks[4]['innerBlocks'][0]['innerBlocks'][0]['innerBlocks'][0]['innerBlocks'][0]['attrs']['post3'],
            $blocks[4]['innerBlocks'][0]['innerBlocks'][0]['innerBlocks'][0]['innerBlocks'][0]['attrs']['post4'],
            $blocks[4]['innerBlocks'][0]['innerBlocks'][0]['innerBlocks'][0]['innerBlocks'][0]['attrs']['post5']
        ]
    ]);
    $params = '';
    $utmSource = '';
    $utmMedium = '';
    if (isset($_GET['utm_source']) && $_GET['utm_source'] !== ''){
        $utmSource = $_GET['utm_source'];
    }
    if (isset($_GET['utm_medium']) && $_GET['utm_medium'] !== ''){
        $utmMedium = $_GET['utm_medium'];
    }
    if ($utmSource !== '' && $utmMedium === '') {
        $params = '?utm_source='.$utmSource;
    }
    if ($utmMedium !== '' && $utmSource === '') {
        $params = '?utm_medium='.$utmMedium;
    }
    if ($utmMedium !== '' && $utmSource !== ''){
        $params = '?utm_source='.$utmSource.'&utm_medium='.$utmMedium;
    }

    $html = '';
    foreach ($posts as $post) {
        $href = get_permalink($post->ID) . $params;
        $imageUrl = esc_url(wp_get_attachment_image_url(get_post_thumbnail_id($post->ID), 'medium'));
        $imageWidth = 120;
        $imageHeight = 82;

        $html .= '<li class="small-images">
            <figure>
                <a href="'.$href.'" target="_blank">
                    <img src="'.$imageUrl.'" alt="'.esc_attr($post->post_title).'" width="'.$imageWidth.'" height="'.$imageHeight.'">
                </a>
            </figure>
            <h5>
                <a href="'.$href.'" title="'.esc_attr($post->post_title).'" target="_blank">'.$post->post_title.'</a>
            </h5>
        </li>';
    }

	return $html;
}

$text = '<cdf:block name="block-detail" content-type="text/html" cache="min">
    <section class="news-list  srpska-info-content">
        <h3 class="section-title__top">
            <a href="https://srpskainfo.com" target="_blank">Srpska Info</a>
        </h3>
        <ul>
            '.printBlicPosts().'
        </ul>
    </section>
</cdf:block>';

//$path = __DIR__ . '/../../../blic-widget.html';
//var_dump(file_put_contents($path, $text));

echo $text;

?>

