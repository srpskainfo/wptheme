<?php /* Template Name: Aklamator Feed */
header("Content-type: text/xml");
echo '<?xml version="1.0" encoding="UTF-8" ?>';
$homeUrl = get_home_url();
global $cache;
$lastModified = $cache->get('lastModifiedAklamator');
if(!$lastModified) {
    $lastModified = date('D, d M Y H:i:s +0000');
}
?>
<rss version="2.0"
     xmlns:content="http://purl.org/rss/1.0/modules/content/"
     xmlns:wfw="http://wellformedweb.org/CommentAPI/"
     xmlns:dc="http://purl.org/dc/elements/1.1/"
     xmlns:atom="http://www.w3.org/2005/Atom"
     xmlns:sy="http://purl.org/rss/1.0/modules/syndication/"
     xmlns:slash="http://purl.org/rss/1.0/modules/slash/"
>

    <channel>
        <title>Srpska Info</title>
        <atom:link href="<?=$homeUrl?>/aklamator-feed/" rel="self" type="application/rss+xml" />
        <link><?=$homeUrl?></link>
        <description></description>
        <lastBuildDate><?=$lastModified?></lastBuildDate>
        <language>bs-BA</language>
        <?php
        require_once(PLUGIN_DIR . 'src/Wp/AklamatorRss/Repository/AklamatorRss.php');
        $posts = unserialize($cache->get('aklamatorPosts'));
        if(count($posts) === 0) {
            global $wpdb;
            $aklamatorRepo = new \GfWpPluginContainer\Wp\AklamatorRss\Repository\AklamatorRss($wpdb);
            $posts = $aklamatorRepo->fetchAklamatorPosts('DESC');
        }


        if(count($posts) > 0 ):?>
           <?php foreach($posts as $post):?>
                <item>
                    <title><?=$post['title']?></title>
                    <link><?=$post['url']?></link>
                    <guid><?=$post['url']?></guid>
                    <description><![CDATA[ <img src="<?=$post['featuredImage']?>"/> ]]></description>
                </item>
           <?php endforeach; ?>
        <?php endif; ?>
    </channel>

</rss>