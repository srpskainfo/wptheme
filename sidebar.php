<?php if ( is_active_sidebar( 'left_sidebar' ) ) : ?>
	<aside class="sidebar-left" role="complementary">
		<?php dynamic_sidebar( 'left_sidebar' ); ?>
	</aside><!-- .sidebar .widget-area -->
<?php endif; ?>