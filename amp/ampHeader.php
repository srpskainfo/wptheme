<?php

use GfWpPluginContainer\Wp\PostHelper;

$logo = get_theme_mod( 'custom_logo' );
$logoImg = wp_get_attachment_image_src( $logo , 'full' );
$page = get_queried_object();
$isSport = false;
$logoWidth = '180';
$logoHeight = '56';
if($page->post_type === 'post') {
    $categories = get_the_category($page->ID);
    foreach($categories as $category) {
        if($category->slug === 'sport') {
            $isSport = true;
            break;
        }
    }
}

if(PostHelper::isSportOrChildPage($page)) {
    $isSport = true;
}
?>
<amp-sidebar id="sidebar"
             class="aside-menu"
             layout="nodisplay"
             side="left">
    <button class="aside-menu__close"
            on="tap:sidebar.close">
        <i class="fa fa-times"></i>
    </button>
    <form data-initialize-from-url
          method="get"
          action="/search"    target="_top"
          class="aside-menu__search">
        <input type="search" name="q" placeholder="Pretraga" data-allow-initialization>
        <button><i class="fa fa-search"></i></button>
    </form>
    <nav>
        <ul>
			<?php
			$menu = wp_get_nav_menu_items('Main Navigation');
			/** @var WP_Post $menuItem */
			foreach ($menu as $menuItem) :?>
                <li><a title="<?= $menuItem->title ?>" href="<?= $menuItem->url ?>"><?= $menuItem->title ?></a></li>
			<?php endforeach; ?>
        </ul>
    </nav>
</amp-sidebar>


<header class="header <?=$isSport ? 'sportHeader' : ''?>">
    <div class="header__top">
        <?php if(!$isSport):?>
        <div class="menu">
            <button on="tap:sidebar.open"
                    class="menu__button">
                <i class="fa fa-bars"></i>
                <i class="fa fa-search"></i>
            </button>
        </div>
        <div class="logo">
            <a href="<?=get_home_url()?>" title="<?=get_bloginfo('name') ?>">
                <span><?='<amp-img src="' . esc_url($logoImg[0]) . '"
                                    alt="' . get_bloginfo( 'name' ) . '" 
                                    width="320" 
                                    height="59" 
                                    layout="responsive" 
                                    title="' . get_bloginfo('name') . '"> 
                          </amp-img>' ?></span>
            </a>
        </div>
        <span></span>
        <?php else :?>
            <div class="dummyDiv">

            </div>
            <div class="logo">
                <div class="backArrow">
                    <i class="fa fa-chevron-left"></i>
                </div>
                <a class="custom-logo-link" title="<?=get_bloginfo( 'name', 'display' )?>" rel="home" href="<?=get_home_url()?>">
                </a>
                <amp-img src="<?=CHILD_THEME_DIR_URI . '/assets/sportLogo.png'?>" alt="<?=get_bloginfo( 'name')?>" width="<?=$logoWidth?>" height="<?=$logoHeight?>">
                </amp-img>
            </div>

                <div class="menu">
                    <button on="tap:sidebar.open"
                            class="menu__button">
                        <i class="fa fa-bars"></i>
                        <i class="fa fa-search"></i>
                    </button>
                </div>
        <?php endif;?>
    </div>
</header>
<main>