<?php
/* Template Name: Search Page */
//if (USE_ELASTIC !== true) {
//    header('Location: ' . home_url('/category' . $_SERVER['REQUEST_URI']));
//    exit();
//}
get_header();
$dotMetricsId = 4595;
$paged = (get_query_var('paged') > 0) ? get_query_var('paged') : 1;
$searchQuery = $_GET['query'];
if (!strlen($searchQuery)) {
    $searchQuery = $_GET['s']; // legacy links
}
if (!$searchQuery) { // prevent landing on this page without search query for now
    wp_safe_redirect(home_url());
    exit();
}
$perPage = PER_PAGE;
$useElastic = true;

//$searchFunctions = new \GfWpPluginContainer\Elastic\Functions($wpdb, $useElastic);
$sortedItems = \GfWpPluginContainer\Indexer\Repository\Article::elasticSearch($searchQuery, $paged);
//Needed for infinite scroll
$ajaxAction = 'search';
$ajaxTermValue = $searchQuery;

$lastPage = ceil( $sortedItems['totalCount'] / PER_PAGE);

if ($paged > $lastPage && $lastPage != 0) {
    $paged = $lastPage;
    wp_safe_redirect(get_permalink().'page/'.$lastPage.'/?query='.$_REQUEST['query'],302);
}

include "templates/archive/archiveDesktop.php";
get_footer();
