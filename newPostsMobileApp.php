<?php
/**
 * Template Name: Mobile App New Posts
 */
use \GfWpPluginContainer\Indexer\Repository\Article as ArticleRepo;

get_header();
$paged = (get_query_var('paged') > 0) ? get_query_var('paged') : 1;
$catUrl = 'sve-vijesti';
$ajaxAction = 'newPosts';
$ajaxTermValue = '';
$dotMetricsId = getDotMetricsId('novo');

//$searchFunctions = new \GfWpPluginContainer\Elastic\Functions($wpdb);
if (USE_ELASTIC !== true) {
    $sortedItems = ArticleRepo::getItemsFromWp([
        'numberposts' => PER_PAGE,
        'orderby' => 'date',
        'order' => 'DESC',
        'suppress_filters' => false,
    ]);
} else {
    $termMock = new \Stdclass();
    $termMock->term_id = 0;
    $sortedItems = ArticleRepo::getItemsFromElasticBy($catUrl, $termMock, $paged, PER_PAGE);
}
include "templates/archive/archiveMobileApp.php";
wp_footer();