<?php

get_header();
$paged = (get_query_var('paged') > 0) ? get_query_var('paged') : 1;
$author = explode('/', $_GET['q'])[2];
$catUrl = '/author/' . $author . '/';
if (strlen($author) === 0) {
    $author = explode('/', $_SERVER['REQUEST_URI'])[2];
}
//Needed for infinite scroll
$ajaxAction = 'author';
$ajaxTermValue = $author;
$dotMetricsId = 4595;
$author = get_user_by('slug', $author);

//$searchFunctions = new \GfWpPluginContainer\Elastic\Functions($wpdb);
// @TODO add fallback to db
$sortedItems = \GfWpPluginContainer\Indexer\Repository\Article::getItemsFromElasticBy('author', $author, $paged, PER_PAGE);

if ($isApp) {
    include "templates/archive/archiveMobileApp.php";
    wp_footer();
} else {

    include "templates/archive/archiveDesktop.php";
	get_footer();
}
