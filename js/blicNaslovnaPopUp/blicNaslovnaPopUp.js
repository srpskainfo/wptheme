//TODO: Move elsewhere
// Get the modal
var modal = document.getElementById("myModal");

// Get the large image and insert it inside the modal, make body unscrollable
var img = document.getElementById("myImg");
var modalImg = document.getElementById("img01");
var body = document.getElementsByTagName("body")[0];
img.onclick = function(){
    modal.style.display = "block";
    modalImg.src = this.getAttribute('data-image-large');
    body.style.overflow = "hidden";
    body.style.height = "100%";
}

// Get the close element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks on close, close the modal, make the body scrollable
span.onclick = function() {
    modal.style.display = "none";
    body.style.overflow = "unset";
    body.style.height = "auto";
}