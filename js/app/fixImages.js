document.addEventListener('DOMContentLoaded', () => {
    if (window.screen.availWidth >= 768) {
        let images = document.querySelectorAll('article figure img');
        images.forEach(image => {
            let src = image.getAttribute('src');
            let targetedWidths = ['427x285', '400x800', '400x600'];
            targetedWidths.forEach(width => {
                let result = src.search(width);
                if (result !== -1) {
                    switch (width) {
                        case '427x285':
                            changeSrcIfImageFound(image, width, ['872x610', '872x581'])
                            break;
                        case '400x800':
                        case '400x600':
                            changeSrcIfImageFound(image, width, ['800x1200'])
                            break;
                    }
                }
            });
        });
    }
})

function changeSrcIfImageFound(image, currentWidth, targets) {
    let resultFound = false;
    targets.forEach(target => {
        let src = image.getAttribute('src');
        let newSrc = src.replace(currentWidth, target);
        fetch(newSrc).then(response => {
            if (response.ok) {
                image.setAttribute('src', newSrc);
                resultFound = true;
            }
        });
    });
    if (resultFound === false) {
        let originalSrc = image.getAttribute('src').replace(`-${currentWidth}`, '');
        fetch(originalSrc).then(response => {
            if (response.ok) {
                image.setAttribute('src', originalSrc);
            }
        });
    }
}