<?php

use Carbon\Carbon;
use GfWpPluginContainer\Wp\PostHelper;

?>
    <script>
        let isEmbedLoaded = false;
        __cmp("addEventListener",["consent", enableVideoPlayerForCmp, false],null);
        function enableVideoPlayerForCmp (event, object) {
            if(isEmbedLoaded) {
                /* No need to pass the data again if the consent is changed and the player is initialized.
                   It will pick up updated settings on next page load. */
                return;
            }
            let loadJS = function(url, callback, location){
                var scriptTag = document.createElement("script");
                scriptTag.src = url;
                scriptTag.onload = callback;
                scriptTag.onreadystatechange = callback;
                location.appendChild(scriptTag);
            };

            let getGdprStatusAsNumber = function (gdpr) {
                if(gdpr === true) return 1;
                return 0;
            }

            let consentData = __cmp("getCMPData");
            // set params for player initialization
            pulse2EmbedConfig.pulsevideo.params.gdpr            = getGdprStatusAsNumber(consentData.gdprApplies);
            pulse2EmbedConfig.pulsevideo.params.gdpr_consent    = consentData.consentstring;
            let callback = function() {};
            loadJS("https://pulsembed.eu/pulsembed.js", callback, document.body);
            isEmbedLoaded = true;
        }
    </script>
<?php
while (have_posts()) : the_post();
    $isSport = PostHelper::isSportOrChildPage($post);
    $categories = get_the_category();
    $catName = $categories[0]->name;
    $catId = $categories[0]->term_id;
    $dotMetricsId = getDotMetricsId($categories[0]->slug);
    $catLink = str_replace('/category', '', get_category_link($categories[0]->term_id));
    if($isSport) {
        foreach ($categories as $category) {
            if ($category->parent === 6) {
                $catName = $category->name;
                $catId = $category->term_id;
                $dotMetricsId = getDotMetricsId($category->slug);
                $catLink = str_replace('/category/sport', '', get_category_link($category->term_id));
            }
        }
    }
    $multipleAuthors = new GfWpPluginContainer\Wp\MultipleAuthors\MultipleAuthors();
    $postOwners = $multipleAuthors->getOwnersForPost(get_the_ID());

    $blocks = count(parse_blocks($post->post_content)) > 0 ? parse_blocks($post->post_content) : [];
    $lead = '';

    if (in_array($blocks[0]['blockName'], ['core/heading', 'core/paragraph'])) {
        $lead = $blocks[0]['innerHTML'];
        $lead = str_replace(['<h2>', '</h2>'], '', $lead);
        unset($blocks[0]);
    }

    $renderedBlocks = [];
    foreach ($blocks as $key => $block) {
        //new format for galleries
        if ($block['blockName'] === 'core/gallery') {
            $ids = implode(',', $block['attrs']['ids']);
            $ligthboxSize = '1500x1500';
            if (wp_is_mobile()) {
                $ligthboxSize = 'single';
                foreach ($block['attrs']['ids'] as $id) {
                    $imageData = wp_get_attachment_metadata($id);
                    if ($imageData['width'] < $imageData['height']){
                        $ligthboxSize = 'portrait-m';
                    }
                }
            }
            $renderedBlocks[] = do_shortcode(sprintf('[gallery ids="%s" lightbox_max_size="%s"]', $ids, $ligthboxSize));
            continue;
        }
        //sets sizes of images for caption image
        if ($block['blockName'] === 'core/image') {
            $imageCaption = mb_strtoupper(wp_get_attachment_caption($block['attrs']['id']));

            if(strlen($imageCaption) && substr($imageCaption,0,4)!=='FOTO') {
                $imageCaption = 'FOTO: ' . $imageCaption;
            }



            $imageLegenda = get_post_meta($block['attrs']['id'], 'legenda', true);
            $imageData = wp_get_attachment_metadata($block['attrs']['id']);
            $imageAltText = get_post_meta($block['attrs']['id'],'altText',true);

            if(!$imageAltText) {
                $imageAltText = $imageCaption;
            }

            if (wp_is_mobile()) {
                $imageSize = 'list-big';
                $imageWidth = '427';
                $imageHeight = '285';
                if ($block['attrs']['sizeSlug'] === 'full') {
                    $imageSize = 'landsape-m';
                    $imageWidth = '400';
                    $imageHeight = '200';
                } else if (isset($imageData['width'],$imageData['height']) && $imageData['width'] < $imageData['height']) {
                    $imageSize = 'portrait-m';
                    $imageWidth = '400';
                    $imageHeight = '800';
                }
            } else {
                $imageSize = 'single';
                $imageWidth = '872';
                $imageHeight = '610';
                if (isset($imageData['width'],$imageData['height']) && $imageData['width'] < $imageData['height']){
                   $imageSize = 'portrait';
                    $imageWidth = '800';
                    $imageHeight = '1200';
                } else if (isset($block['attrs']['sizeSlug']) && $block['attrs']['sizeSlug'] === 'full') {
                    $imageSize = 'full';
                    $imageWidth = $imageData['width'];
                    $imageHeight = $imageData['height'];
                }
            }

            $imageUrl = wp_get_attachment_image_url($block['attrs']['id'], $imageSize);
            $block = '<figure class="captionImageWrapper"><img src="' . $imageUrl . '" alt="' . esc_attr($imageAltText) . '" width="' . $imageWidth . '" height="' . $imageHeight . '" />
        <figcaption class="captionImageCaption">' . $imageCaption . '</figcaption>
        </figure>
        <span class="keySingle">' . $imageLegenda . '</span>';
        } else {
            $block = apply_filters('the_content', render_block($block));
            $block = str_replace(['<br/>', '<br />', '<br>'], '', $block);

            // detect lead type ( check for h2, h3, h4 ) if not found then extract first <p> tag
            if (!strlen($lead)) {
                $start = mb_strpos($block, '<p>');
                $end = mb_strpos($block, '</p>', $start);
                $lead = mb_substr($block, $start, $end - $start + 4);
            }
        }

        $renderedBlocks[] = str_replace($lead, '', $block);
    }

    $featuredImageId = get_post_thumbnail_id($post->ID);

    if(!$featuredImageId){
        $featuredImageId = get_option("defaultFeaturedImage");
    }

    if (wp_is_mobile()) {
        $imageUrl = esc_url(wp_get_attachment_image_url($featuredImageId, 'list-big'));
        $imageWidth = 427;
        $imageHeight = 285;
    } else {
        $imageUrl = esc_url(wp_get_attachment_image_url($featuredImageId, 'single'));
        $imageWidth = 872;
        $imageHeight = 610;
    }
    $imageLegenda = get_post_meta($featuredImageId, 'legenda', true);
    $title = $post->post_title;

    // Post dates

    $publishedDate = new Carbon($post->post_date);
    $publishedDateString = str_replace(['May', 'Aug', 'Oct'], ['Maj', 'Avg', 'Okt'], $publishedDate->format('d.M,Y.'));

//    $publishedDate->locale('sr'); // does not work ???
//    Carbon::setLocale('sr');
//    echo strftime('%d.%h,%G.', strtotime($publishedDate->toDateTimeString())) . PHP_EOL;
//    echo $publishedDate->formatLocalized('%d.%h,%G.');
    $updatedDate = new Carbon($post->post_modified);

    $featuredImageCaption = mb_strtoupper(get_the_post_thumbnail_caption($post->ID));

    if(strlen($featuredImageCaption) && substr($featuredImageCaption,0,4)!=="FOTO"){
        $featuredImageCaption = "FOTO: " . $featuredImageCaption;
    }
    $imageAltTextFeatured = get_post_meta($featuredImageId, 'altText', true);
    if(!$imageAltTextFeatured) {
        $imageAltTextFeatured = $title;
    }

    ?>
    <article class="article">
    <header class="article__top <?= $isSport ? 'articleSportTop' : ''?>">
        <div class="article__top-content">
            <?php if($catName !== 'HPG'):?>
                <a class="categoryPageNameLink" href="<?= $catLink ?>" title="<?= $catName ?>"><?= $catName ?></a>
            <?php else:?>
                <span class="categoryPageNameLink"><?=$catName?></span>
            <?php endif;?>
            <h1><?= $title ?></h1>
            <p><?= strip_tags($lead, '<strong><h2><h3><h4>'); ?></p>
        </div>
        <figure class="singleFeaturedImageWrapper">
            <?php
                if($post->ID === 1732106) {
                    $imageUrl = 'https://srpskainfo.com/wp-content/uploads/2023/06/Sta%C5%A1a-Ko%C5%A1arac-Foto-Ministarstvo-spoljne-trgovine-i-ekonomskih-odnosa-BiH.jpg';
                }
            ?>
            <img src="<?= $imageUrl ?>" alt="<?= esc_attr($imageAltTextFeatured) ?>" width="<?= $imageWidth ?>" height="<?= $imageHeight ?>"/>
            <figcaption><?= $featuredImageCaption ?></figcaption>
        </figure>
        <div class="keyFeatured">
            <span><?= $imageLegenda ?></span>
        </div>
    </header>
    <?php
    if (wp_is_mobile()) {
        dynamic_sidebar('category_feed_baner_mobile_1');
    }
    ?>
    <div class="article__center <?= $isSport ? 'articleSport' : ''?>">
        <section class="article__left">
            <aside class="article__aside-left">
                <div class="author">
                    <?php if (count($postOwners) > 1 && count($postOwners) !== 0): ?>
                    <span>Autori:</span>
                    <?php else:?>
                    <span>Autor:</span>
                    <?php endif;
                    $i = 1;
                    $comma = ',';
                    $count = count($postOwners);
                    /** @var \GfWpPluginContainer\Wp\MultipleAuthors\Model\GfPostOwner $postOwner */
                    foreach($postOwners as $postOwner):
                    if ($i === count($postOwners)) {
                        $comma = '';
                    }
                        $i++;
                        $ownerLink = get_author_posts_url($postOwner->getAuthorId());
                        $ownerDisplayName = $postOwner->getAuthorDisplayName();
                    ?>
                        <a href="<?=$ownerLink?>" title="<?=$ownerDisplayName?>"><?=$ownerDisplayName?></a><?=$comma?>
                    <?php endforeach; ?>
                </div>
                <time datetime="<?= $publishedDate->toDateTimeString() ?>">
                    <div class="date"><?= $publishedDateString ?> </div>
                    <div class="time">
                        <i class="fas fa-circle"></i>
                        <?= $publishedDate->toTimeString('minute') ?>
                        <?php
                        if ($publishedDate->format('Y/m/d/H:i') < $updatedDate->format('Y/m/d/H:i')) :?>
                            <i class="fas fa-arrow-right"></i>
                            <?= $updatedDate->toTimeString('minute') ?>
                        <?php endif; ?>
                    </div>
                </time>
                <div class="social">
                    <?php dynamic_sidebar('post_social_sidebar'); ?>
                    <a class="a2a_button_viber" title="Viber" rel="nofollow noopener" href="viber://forward?text=<?=get_permalink(get_the_ID())?><?=urlencode('?utm_source=viber&utm_medium=social&utm_campaign=share-button')?>">
                        <span class="a2a_svg a2a_s__default a2a_s_viber" style="background-color: transparent;">
                            <svg focusable="false" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32"><path d="M22.57 27.22a7.39 7.39 0 0 1-1.14-.32 29 29 0 0 1-16-16.12c-1-2.55 0-4.7 2.66-5.58a2 2 0 0 1 1.39 0c1.12.41 3.94 4.3 4 5.46a2 2 0 0 1-1.16 1.78 2 2 0 0 0-.66 2.84A10.3 10.3 0 0 0 17 20.55a1.67 1.67 0 0 0 2.35-.55c1.07-1.62 2.38-1.54 3.82-.54.72.51 1.45 1 2.14 1.55.93.75 2.1 1.37 1.55 2.94a5.21 5.21 0 0 1-4.29 3.27zM17.06 4.79A10.42 10.42 0 0 1 26.79 15c0 .51.18 1.27-.58 1.25s-.54-.78-.6-1.29c-.7-5.52-3.23-8.13-8.71-9-.45-.07-1.15 0-1.11-.57.05-.87.87-.54 1.27-.6z" fill="#0c0c0c" fill-rule="evenodd"></path><path d="M24.09 14.06c-.05.38.17 1-.45 1.13-.83.13-.67-.64-.75-1.13-.56-3.36-1.74-4.59-5.12-5.35-.5-.11-1.27 0-1.15-.8s.82-.48 1.35-.42a6.9 6.9 0 0 1 6.12 6.57z" fill="#0c0c0c" fill-rule="evenodd"></path><path d="M21.52 13.45c0 .43 0 .87-.53.93s-.6-.26-.64-.64a2.47 2.47 0 0 0-2.26-2.43c-.42-.07-.82-.2-.63-.76.13-.38.47-.41.83-.42a3.66 3.66 0 0 1 3.23 3.32z" fill="#0c0c0c" fill-rule="evenodd"></path></svg>
                        </span>
                    </a>
                </div>
            </aside>
            <div class="article__content">
                <?php
                $bannerPosition = 1;
                $validBlock = 1;
                foreach ($renderedBlocks as $key => $block) {
                    if ($validBlock === 1 && strlen($block)) {
                        $block = html_entity_decode($block, ENT_COMPAT, 'UTF-8');
                        $block = str_replace_once(html_entity_decode('&ndash;', ENT_COMPAT, 'UTF-8'), '', $block);
                        $block = str_replace_once(html_entity_decode('&mdash;', ENT_COMPAT, 'UTF-8'), '', $block);
                    }

                    if ($validBlock === 1 && isset($block[4]) && (in_array($block[4], ['&', '-']))) {
                        $block = str_replace_once(['&#8211;', '-'], '', $block);
                    }
                    echo $block;

                    if (strlen($block)) {
                        $validBlock++;
                    }

                    if ($validBlock % 5 === 0) {
                        if (wp_is_mobile()) {
                            dynamic_sidebar('single_intext_feed_baner_mobile_' . $bannerPosition);
                        } else {
                            dynamic_sidebar('single_intext_feed_baner_' . $bannerPosition);
                        }
                        $bannerPosition++;
                    }
                } ?>
                <div class="article__tags">
                    <?= the_tags('', '', '') ?>
                </div>
                <a style="font-weight:bold;
                font-size:14px;margin-bottom:0.6rem;display:block; width:max-content;background-color:#7360F2;color:white;padding:0.6rem;border-radius:5px;cursor:pointer;"
                   title="Viber grupa"
                   target="_blank"
                   href="https://invite.viber.com/?g2=AQADj2jdM9Z9glCzKoVL14kr2VhoO%2FPqOu0JB5T7bD3knZHJFKQs4T3Pr1gHQzZv&lang=en">
                    Najnovije vijesti Srpskainfo i na Viberu
                </a>
                <?php if (wp_is_mobile()) {
                    dynamic_sidebar('single_post_bottom_banner_mobile');
                } else {
                    dynamic_sidebar('single_post_bottom_banner');
                } ?>
                <div id="comments-container">

                </div>
            </div>
            <footer class="article__bottom">
                <div class="article__comments container">
                    <div class="container">
                        <p>Možete da nas pratite i na Facebook stranici:</p>
                        <div class="fb__page" style="display: block; margin: auto;"></div>
                    </div>
                </div>
                <?php if (wp_is_mobile()) {
                    dynamic_sidebar('single_post_bottom_mobile');
                } else {
                    dynamic_sidebar('single_post_bottom');
                } ?>
            </footer>
        </section>

        <aside class="article__right">
            <?php if (wp_is_mobile()) {
                dynamic_sidebar('single_page_sidebar_mobile');
                echo '<div class="widgetAreaMobile">';
                dynamic_sidebar('single_page_widget_area_mobile');
                echo '</div>';
            } else {
                dynamic_sidebar('single_page_sidebar_desk');
            } ?>
        </aside>
    </div>

<?php endwhile; ?>
    </article>
<?php if (!is_user_logged_in()): ?>
    <script>
        jQuery(document).ready(function (){
            setAjaxViewCount()
        })
        function setAjaxViewCount(){
            jQuery.ajax({
                url: "<?=admin_url(). 'admin-ajax.php'?>",
                type: 'POST',
                data: "action=setAjaxViewCount&postId=<?=$post->ID?>",
                success: function (response) {
                },
                error: function () {
                }
            });
        }
    </script>
<?php endif ?>
    <script type="text/javascript">
        var commentingPlatformConfig = {
            env: "prod", // "env" varijabla moze da ima 2 vrednosti - "prod" za produkcioni sajt i  "staging" za staging sajt - zavisnosti od ovog parametra se uctiva razlicit CSS
            endpoint: "https://komentari.srpskainfo.com", // o ovome cu posebno u mejlu da objasnim
            // staging - https://komentari.srpska.greenfriends.systems
            tenantId: "srpskainfo", // SI tenant
            categoryName: "<?=$catName ?? ''?>", // categoryName se pravi na sledeci nacin "Category / SubCategory (/ SubSubCategory)"
            categoryId: "<?=$catId ?? ''?>", // identifikator kategorije kojoj artikal pripada
            storyId: "<?=$post->ID?>", // identifikator artikla
            storyTitle: "<?=htmlentities($post->post_title)?>", // naslov artikla
            storyUrl: "<?=get_permalink($post->ID)?>", // url artikla
            storyPublicationId: "<?=$post->ID?>", // identifikator artikla
            storyPublicationDate: "<?=$post->post_date?>", // vreme objave artikla
            limit: 50 // limit
        };

        // produkcioni javascript
        var cpScriptUrl = "https://ocdn.eu/blic/commenting-platform-fe/main.js"
        // staging javascript - https://ocdn.eu/blic/commenting-platform-fe/staging/main.js
        // drugaciji javascript se ucitava u zavisnosti od okruzenja (staging/produkcija)
        document.addEventListener('DOMContentLoaded', (e) => {
            var script=document.createElement("script");script.type="text/javascript";var version=localStorage.getItem("cp-app-ver")||"0.0.0";script.src=cpScriptUrl+"?v="+version,document.getElementsByTagName("body")[0].appendChild(script);
        });
    </script>
    <script>
        function my_addtoany_onshare(share_data) {
            let oldUrl = share_data.url;
            oldUrl =  oldUrl.replace('?utm_source=facebook&utm_medium=social&utm_campaign=share-button','');
            oldUrl =  oldUrl.replace('?utm_source=twitter&utm_medium=social&utm_campaign=share-button','');
            oldUrl = oldUrl.replace('?utm_source=whatsapp&utm_medium=social&utm_campaign=share-button','');
            let newUrl = '';
            if(share_data.serviceCode && oldUrl && !oldUrl.includes('utm_campaign=share-button')) {
                switch(share_data.serviceCode) {
                    case 'facebook':
                        newUrl = oldUrl + '?utm_source=facebook&utm_medium=social&utm_campaign=share-button';
                        break;
                    case 'twitter':
                        newUrl = oldUrl + '?utm_source=twitter&utm_medium=social&utm_campaign=share-button';
                        break;
                    case 'whatsapp':
                        newUrl = oldUrl + '?utm_source=whatsapp&utm_medium=social&utm_campaign=share-button';
                        break;
                }
            } else {
                return {url:oldUrl};
            }

            // Modify the share by returning an object
            // with a "url" property containing the new URL
            if (newUrl != oldUrl) {
                return {
                    url: newUrl,
                };
            }

        }

        a2a_config.callbacks = a2a_config.callbacks || [];
        a2a_config.callbacks.push({
            share: my_addtoany_onshare,
        });
    </script>
<?php
function str_replace_once($str_pattern, $str_replacement, $string)
{
    if (!is_string($str_pattern) && !is_int($str_pattern)) {
        return $string;
    }
    $occurrence = strpos($string, $str_pattern);
    if ($occurrence !== false) {
        return substr_replace($string, $str_replacement, $occurrence, strlen($str_pattern));
    }

    return $string;
}