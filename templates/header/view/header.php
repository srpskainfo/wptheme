<?php
// Set the width and image to be used in <img> width and height attributes. We only use two cases, desktop and mobile
use GfWpPluginContainer\Wp\PostHelper;

$logoWidth = '320';
$logoHeight = '59';
$isSport = false;
$page = get_queried_object();
if($page->post_type === 'post') {
    $categories = get_the_category($page->ID);
    foreach($categories as $category) {
        if($category->slug === 'sport') {
            $isSport = true;
            break;
        }
    }
}

if(PostHelper::isSportOrChildPage($page)) {
    $isSport = true;
}

if (wp_is_mobile()) {
    $logoWidth = '180';
    $logoHeight = '40';
}
?>
<header class="header <?=$isSport ? 'sportHeader' : ''?>">
    <?php if($isSport) :?>
    <div class="sportTopBar">
        <div class="social">
            <ul>
                <?php dynamic_sidebar('top_bar_right'); ?>
            </ul>
        </div>
    </div>
    <?php endif;?>
    <div class="header__top">
        <?php if(!$isSport):?>
            <div class="menu">
            <span class="menu__button">
                <i class="fas fa-bars"></i>
                <i class="fas fa-search"></i>
            </span>
            </div>
            <div class="logo">
                <?php
                $logoUrl = get_home_url();
                $logoSrc = get_theme_mod('gf_logo');
                ?>
                <a class="custom-logo-link" title="<?=get_bloginfo( 'name', 'display' )?>" rel="home" href="<?=$logoUrl?>">
                    <img class="custom-logo" src="<?=$logoSrc?>" alt="<?= get_bloginfo('name') ?>" width="<?=$logoWidth?>" height="<?=$logoHeight?>">
                </a>
            </div>
            <div class="social">
                <ul>
                    <?php dynamic_sidebar('top_bar_right'); ?>
                </ul>
            </div>
        <?php else :?>
        <div class="dummyDiv">

        </div>
        <div class="logo">
            <div class="backArrow">
                <i class="fas fa-chevron-left"></i>
            </div>
            <a class="custom-logo-link" title="<?=get_bloginfo( 'name', 'display' )?>" rel="home" href="<?=get_home_url()?>">
            </a>
            <img class="custom-logo" src="<?=CHILD_THEME_DIR_URI . '/assets/sportLogo.png'?>" alt="Srpskainfo sport" width="<?=$logoWidth?>" height="<?=$logoHeight?>">
        </div>
        <div class="menu">
            <span class="menu__button">
                <i class="fas fa-bars"></i>
                <i class="fas fa-search"></i>
            </span>
        </div>
        <?php endif;?>
    </div>
    <div class="header__bottom <?= $isSport ? 'categoryHeader' : ''?>">
        <div class="container containerHeader">
            <nav>
                <ul>
                <?php
                $key = 'navigation-desktop#Main Navigation';
                $html = $cache->get($key);
                if ($html === false) {
                    $menu = wp_get_nav_menu_items('Main Navigation');
                    $specialCategory = null;
                    $html = '';
                    /** @var WP_Post $menuItem */
                    foreach ($menu as $menuItem) {
                        if($menuItem->title === 'Sport') {
                            $html .= '<li class="mainNavSportLink"><a title="' . esc_attr($menuItem->title) . '" href="' . $menuItem->url . '">' . $menuItem->title . '</a></li>';
                        } else {
                            $html .= '<li><a class="navigationHeaderLink" title="' . esc_attr($menuItem->title) . '" href="' . $menuItem->url . '">' . $menuItem->title . '</a></li>';
                        }
                    }
                    $cache->set($key, $html, 300);
                }
                echo $html;
                /**
                 * This category has a logo instead of text in nav menu and it has to be last
                 *
                 */
                ?>
                    <li class="sponsor">
                        <a title="Digitalna Srpska" href="/digitalna-srpska/"><img src="<?=get_theme_mod('gf_side_logo')?>" alt="Digitalna Srpska" title="Digitalna Srpska" width="80" height="60">
                        </a>
                        <a href="https://mtel.ba/" title="Mtel" target="_blank">
                            <div class="sponsor__logo"><span>By</span> <img src="<?= CHILD_THEME_DIR_URI . '/images/logo-mtel.svg' ?>" alt="By Mtel" width="40" height="40"/></div>
                        </a>
                    </li>
                </ul>
            </nav>
        </div>
    </div>
</header>
<?php
if($isSport):
    $subCategories = get_categories(['parent' => 6]);
    $current = 'sport';
    if($page->post_type === 'page' && $page->post_name !== 'sport') {
        $current = $page->post_name;
    }
    if($page->post_type === 'post') {
        $cats = get_the_category($page->ID);
        foreach($cats as $cat) {
            if($cat->parent !== 0) {
                $current = $cat->slug;
            }
        }
    }
    ?>
    <nav class="subNav">
        <ul>
            <li style="order:1;">
                <a class="<?=$current === 'sport' ? 'highlight' : ''?>" id="homeLink" href="<?=get_home_url() . '/sport'?>" title="srpskainfo">Naslovna</a>
            </li>
            <?php foreach($subCategories as $subCategory):?>
                <?php if($subCategory->slug === 'ostalo') {
                    $class = $current === $subCategory->slug ? 'highlight' : '';
                    $lastItem = sprintf('<li style="order:6;"><a class="' . $class . '" href="%s" title="%s">%s</a></li>',
                        '/' . $subCategory->slug . '/', $subCategory->name,$subCategory->name);
                    continue;
                } ?>
                <?php if($subCategory->slug === 'qatar-2022'): ?>
                    <li class="specialSubSportCategoryLink">
                        <a class="<?=$current === $subCategory->slug ? 'highlight' : ''?>" href="<?='/' . $subCategory->slug . '/'?>" title="<?=$subCategory->name?>">
                            <?='Qatar 2022'?>
                        </a>
                    </li>
                    <?php continue; ?>
                <?php endif;?>
                <?php
                $style = '';
                switch($subCategory->slug) {
                    case 'fudbal':
                        $style = 'style="order:3;"';
                        break;
                    case 'kosarka':
                        $style = 'style="order:4;"';
                        break;
                    case 'tenis':
                        $style = 'style="order:5;"';
                        break;
                    case 'ostalo':
                        $style = 'style="order:6;"';
                        break;
                }
                ?>
                <?php if($subCategory->slug === 'fudbal'): ?>

                <?php endif;?>
                <li <?=$style?> <?=$subCategory->slug === 'fudbal' ? 'class="hiddenSubItem"' : ''?>>
                    <a class="<?=$current === $subCategory->slug ? 'highlight' : ''?>" href="<?='/' . $subCategory->slug . '/'?>" title="<?=$subCategory->name?>"><?=$subCategory->name?></a>
                </li>
            <?php endforeach;?>
            <?= $lastItem ?? '' ?>
        </ul>
    </nav>
<?php endif;?>
<main>