<?php
$page = get_queried_object();
$isSport = \GfWpPluginContainer\Wp\PostHelper::isSportOrChildPageApp($page);
?>

<header class="header <?= $isSport ? 'sportHeader' : ''?>">
    <?php if(!$isSport):?>
        <div class="header__top">
            <div class="menu">
                <span class="menu__button">
                    <i class="fas fa-bars"></i>
                    <i class="fas fa-search"></i>
                </span>
            </div>
            <?php if ( in_array( $_SERVER['REQUEST_URI'], [ '/mobile-home/', '/?isapp=true' ] ) ) {
                $onclick = "window.location.reload(true)";
            } else {
                $onclick = "window.location.href='" . get_home_url() . "/mobile-home'";
            } ?>

            <div class="logo" onclick="<?= $onclick ?>">
                <a class="custom-logo-link" title="<?= get_bloginfo( 'name', 'display' ) ?>" rel="home">
                    <img class="custom-logo" src="<?= get_theme_mod( 'gf_logo' ) ?>" alt="<?= get_bloginfo( 'name' ) ?>" width="180" height="33">
                </a>
            </div>
            <?php if ( ! strpos( $_SERVER['REQUEST_URI'], 'mobile-home' ) !== false ) : ?>
                <div class="backButtonWrapper">
                    <i id="bckbtn" style="height:22px;width:26px; color:black;" onclick="window.history.back()" class="fas fa-chevron-left"></i>
                </div>
                <script>
                    if (history.length < 2) {
                        document.getElementById("bckbtn").setAttribute("onClick", "window.location.href='https://srpskainfo.com/mobile-home/'");

                        window.history.pushState("", "", "#");
                        window.addEventListener("popstate", function () {

                            history.replaceState(null, document.title, window.location.pathname);
                            setTimeout(function () {
                                window.location.replace("http://srpskainfo.com/mobile-home/");
                            }, 0);

                        }, false);
                    }
                </script>
            <?php endif; ?>
            <div class="reloadButtonWrapper">
                <img onclick="window.location.reload(true)" src="<?= PARENT_THEME_DIR_URI . '/images/reload.svg' ?>"
                     alt="reload"
                     title="reload"
                     style="height: 33px;width: 33px">
            </div>
        </div>
    <?php else:?>
        <div class="header__top">
            <?php if ( in_array( $_SERVER['REQUEST_URI'], [ '/mobile-home/', '/?isapp=true' ] ) ) {
                $onclick = "window.location.reload(true)";
            } else {
                $onclick = "window.location.href='" .  get_home_url() . "/app/?url=sport&type=page'";
            } ?>
            <div class="dummyDiv appDummyDiv">

            </div>
            <div class="logo">
                <div class="backArrow">
                    <i class="fas fa-chevron-left"></i>
                </div>
                <a class="custom-logo-link" title="<?= get_bloginfo( 'name', 'display' ) ?>" rel="home" href="/mobile-home"></a>
                    <img class="custom-logo" src="<?=CHILD_THEME_DIR_URI . '/assets/sportLogo.png'?>" alt="<?= get_bloginfo( 'name' ) ?>" width="180" height="33">
            </div>
            <?php if ( ! strpos( $_SERVER['REQUEST_URI'], 'mobile-home' ) !== false ) : ?>
                <div class="backButtonWrapper">
                    <i id="bckbtn" style="height:16px;width:16px; color:black;" onclick="window.history.back()" class="fas fa-chevron-left"></i>
                </div>
                <script>
                    if (history.length < 2) {
                        document.getElementById("bckbtn").setAttribute("onClick", "window.location.href='https://srpskainfo.com/mobile-home/'");

                        window.history.pushState("", "", "#");
                        window.addEventListener("popstate", function () {

                            history.replaceState(null, document.title, window.location.pathname);
                            setTimeout(function () {
                                window.location.replace("http://srpskainfo.com/mobile-home/");
                            }, 0);

                        }, false);
                    }
                </script>
            <?php endif; ?>
            <div class="reloadButtonWrapper">
                <i class="fas fa-redo" onclick="window.location.reload(true)" style="color:black; height: 15px;width: 15px"></i>
            </div>
            <div class="menu">
                <span class="menu__button">
                    <i class="fas fa-bars"></i>
                    <i class="fas fa-search"></i>
                </span>
            </div>
        </div>
    <?php endif;?>
</header>
<nav class="subNav">
    <ul>
        <?php
        if($isSport):
            $current = '';
            if($_GET['url']) {
                $current = $_GET['url'];
            }
            if($page->post_type === 'post') {
                $current = 'sport';
                $cats = get_the_category($page->ID);
                foreach($cats as $cat) {
                    if($cat->parent !== 0) {
                        $current = $cat->slug;
                    }
                }
            }
            ?>
            <li style="order:1;" onclick="<?="window.location.href='" . get_home_url() . "/app/?url=sport&type=page'"?>">
                <a class="<?= $current === 'sport' ? 'highlight' : ''?>" id="homeLink" title="srpskainfo">Naslovna</a>
            </li>
        <?php
        $subCategories = get_categories(['parent' => 6]);
        foreach($subCategories as $subCategory):
            $catUrl = parseAppUrl('page','/' . $subCategory->slug . '/');
            ?>
            <?php if($subCategory->slug === 'ostalo') {
                $lastItem = sprintf('<li style="order:6;"><a class="%s" href="%s" title="%s">%s</a></li>',
                    $current === 'ostalo' ? 'highlight' : '',$catUrl, $subCategory->name,$subCategory->name);
                continue;
            } ?>
            <?php if($subCategory->slug === 'qatar-2022'): ?>
            <li class="specialSubSportCategoryLink">
                <a class="<?=$current === $subCategory->slug ? 'highlight' : ''?>" href="<?=$catUrl?>" title="<?=$subCategory->name?>">
                    <?='Qatar 2022'?>
                </a>
            </li>
            <?php continue; ?>
        <?php endif;?>
            <?php
            $style = '';
            switch($subCategory->slug) {
                case 'fudbal':
                    $style = 'style="order:3;"';
                    break;
                case 'kosarka':
                    $style = 'style="order:4;"';
                    break;
                case 'tenis':
                    $style = 'style="order:5;"';
                    break;
                case 'ostalo':
                    $style = 'style="order:6;"';
                    break;
            }
            ?>
            <?php if($subCategory->slug === 'fudbal'): ?>

        <?php endif;?>
            <li <?=$style?> <?=$subCategory->slug === 'fudbal' ? 'class="hiddenSubItem"' : ''?>>
                <a class="<?=$current === $subCategory->slug ? 'highlight' : ''?>" href="<?=$catUrl?>" title="<?=$subCategory->name?>"><?=$subCategory->name?></a>
            </li>
        <?php endforeach;?>
        <?= $lastItem ?? '' ?>
        <?php endif;?>
    </ul>
</nav>
<main>
