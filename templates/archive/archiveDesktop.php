<?php use Carbon\Carbon;
use GfWpPluginContainer\Wp\PostHelper;

global $cache;
/*
 * When infinite scroll function is triggered this is set to true and its used to change class
 *  of items printed via infinite scroll
 */
$infiniteScroll = false;
$post = get_post();
$sportCat = false;
$restrictedSportPosts = [];
if(PostHelper::isSportOrChildPage($post)) {
    $sportCat = true;
}
dynamic_sidebar( 'category_page_top_banner' ); ?>
<!-- Container Start -->
<div class="container <?= $sportCat ? 'sportContainer' : ''?>">
    <!-- siCategory Start -->
    <div class="siCategory">
        <div class="category__title <?= $sportCat ? 'sportTitle' : ''?>">
        <?php if (isset($searchQuery)): ?>
            <h1><?= sprintf('Rezultati pretrage za: %s', $searchQuery) ?></h1>
        <?php endif ?>
        <?php if ( is_author() ): ?>
            <h1><?= get_the_author() ?></h1>
        <?php endif ?>
        <?php if (isset($catName) && $post->post_title !== 'Sport' && !$sportCat && wp_is_mobile()) :?>
            <h1><span>#</span><?=ucfirst($catName);?></h1>
        <?php endif ?>
        </div>
        <div class="category__left">
            <!-- Category Left Side Start -->
            <section class="news items__2 <?= $sportCat ? 'sportCategoryPage' : ''?>">
                <?php
                if($sportCat && !str_contains($_SERVER['REQUEST_URI'],'/page/')) {
                    if (has_blocks($post->post_content)) {
                        $blocks = parse_blocks($post->post_content);
                        foreach($blocks as $block) {
                            echo apply_filters('the_content', render_block($block));
                        }
                        if($post->post_title === 'Sport') {
                            $restrictedFromCache = unserialize($cache->get('restrictedSportPosts'),['allowed_classes' => false]);
                            if($restrictedFromCache) {
                                $restrictedSportPosts = $restrictedFromCache;
                            }
                        }
                    }
                }
                ?>
                <?php
                    $lastPage = ceil( $sortedItems['totalCount'] / PER_PAGE);
                    if ( count( $sortedItems['articles'] ) ) {
                        /* @var \GfWpPluginContainer\Wp\Article $article */
                        $bannerPosition = 1;
                        $sportKeyReductionForSkippedPosts = 0;
                        foreach ( $sortedItems['articles'] as $key => $article ) {
                            if(in_array($article->getPostId(),$restrictedSportPosts,false)) {
                                $sportKeyReductionForSkippedPosts++;
                                continue;
                            }
                            $post = [];
                            $blocks       = count(parse_blocks($article->getBody())) > 0 ? parse_blocks($article->getBody()) : null;
                            $permalink    = esc_url($article->getPermalink());
                            $thumbnailSrc = esc_url($article->getThumbnail());
                            $featuredImageUrl=(get_post_thumbnail_id($article->getPostId()) ? : get_option("defaultFeaturedImage"));

                            if (wp_is_mobile()) {
                                if ($key < 1) {
                                    $post['imageUrl'] = esc_url(wp_get_attachment_image_url($featuredImageUrl, 'large'));
                                    $post['imageWidth'] = 374;
                                    $post['imageHeight'] = 250;
                                } else {
                                    $post['imageUrl'] = esc_url(wp_get_attachment_image_url($featuredImageUrl, 'medium'));
                                    $post['imageWidth'] = 120;
                                    $post['imageHeight'] = 82;
                                }

                            } else {
                                $post['imageUrl'] = esc_url(wp_get_attachment_image_url($featuredImageUrl, 'list-big'));
                                $post['imageWidth'] = 427;
                                $post['imageHeight'] = 285;
                            }
                            $title        = esc_html($article->getTitle());
                            $post['imageAltText'] = esc_attr(
                                get_post_meta(get_post_thumbnail_id($article->getPostId()), 'altText', true)
                            );
                            if (!$post['imageAltText'] || $post['imageAltText'] === '') {
                                $post['imageAltText'] = $title;
                            }


                            if($article->hasSecondLevelCategory()) {
                                foreach($article->getCategory() as $articleCat) {
                                    if($sportCat) {
                                        if ($articleCat['level'] === 2) {
                                            $catNameFromArticle = $articleCat['name'];
                                            $catSlug = $articleCat['slug'];
                                        }
                                    } elseif($articleCat['level'] === 1) {
                                        $catNameFromArticle = $articleCat['name'];
                                        $catSlug = $articleCat['slug'];
                                    }
                                }
                            } else {
                                $catNameFromArticle = $article->getCategory()['name'];
                                $catSlug = $article->getCategory()['slug'];
                            }
                            $catName = mb_strtolower($catNameFromArticle);
                            $catUrl = '/' . $catSlug . '/';
                            // If the category name is kolumne get the author information and display it in the template
                            if ($catName === 'kolumne') {
                                $postId = $article->getPostId();
                                $authors = $article->getAuthor();
                                $authorName = $article->getAuthor()[0]['name'];
                                $authorUrl = get_author_posts_url($article->getAuthor()[0]['id']);
                            }
                            Carbon::setLocale('bs');
                            $postDate = new Carbon($article->getPublishedAt(), new \DateTimeZone('Europe/Sarajevo'));
                            $timeAgo = explode(' ', $postDate->longAbsoluteDiffForHumans());
                            include(__DIR__ . '/../loop/categoryArticle.php');

                            if (wp_is_mobile()) {
                                if($sportCat) {
                                    if (($key+1-$sportKeyReductionForSkippedPosts) % 3 === 0) {
                                        dynamic_sidebar('category_feed_baner_mobile_' . $bannerPosition);
                                        $bannerPosition++;
                                    }
                                } else {
                                    if (($key+1) % 3 === 0) {
                                        dynamic_sidebar('category_feed_baner_mobile_' . $bannerPosition);
                                        $bannerPosition++;
                                    }
                                }

                            } else {
                                if($sportCat) {
                                    if (($key+1-$sportKeyReductionForSkippedPosts) % 4 === 0) {
                                        dynamic_sidebar('category_feed_baner_' . $bannerPosition);
                                        $bannerPosition++;
                                    }
                                } else {
                                    if (($key+1) % 4 === 0) {
                                        dynamic_sidebar('category_feed_baner_' . $bannerPosition);
                                        $bannerPosition++;
                                    }
                                }

                            }
                        }
                    } ?>
            </section>
            <div class="siPagination">
                <?php \GfWpPluginContainer\Pagination\Pagination::dottedPagination( $lastPage, $paged ); ?>
            </div>
        </div><!-- Category Left Side End  -->

        <aside class="category__right"> <!-- Category Right Side -->
            <?php if (wp_is_mobile()) {
                if($sportCat) {
                    dynamic_sidebar('sport_page_sidebar_mobile');
                } else {
                    dynamic_sidebar('category_page_sidebar_mobile');
                }
            } else {
                if($sportCat) {
                    dynamic_sidebar('sport_page_sidebar');
                } else {
                    dynamic_sidebar('category_page_sidebar');
                }
            } ?>
        </aside><!-- Category Right Side End -->
    </div><!-- siCategory End -->
</div><!-- Container End -->
<script type="text/javascript">
    var commentingPlatformConfig = {
        endpoint: "https://komentari.srpskainfo.com", // o ovome cu posebno u mejlu da objasnim
        // staging - https://komentari.srpska.greenfriends.systems
        tenantId: "srpskainfo", // SI tenant
    };
    // produkcioni javascript
    var cpScriptUrl = "https://ocdn.eu/blic/commenting-platform-fe/main.js"
    // staging javascript - https://ocdn.eu/blic/commenting-platform-fe/staging/main.js
    // drugaciji javascript se ucitava u zavisnosti od okruzenja (staging/produkcija)
    document.addEventListener('DOMContentLoaded', (e) => {
        var script=document.createElement("script");script.type="text/javascript";var version=localStorage.getItem("cp-app-ver")||"0.0.0";script.src=cpScriptUrl+"?v="+version,document.getElementsByTagName("body")[0].appendChild(script);
    });
</script>