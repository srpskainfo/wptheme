<?php $class = $infiniteScroll ? 'infScroll': ''?>
<article class="news__item <?=$class?>">
    <a title="<?=esc_attr($title)?>" href="<?=$permalink?>">
        <figure><img class="lazy-loaded" data-lazy-type="image" data-src="<?=$post['imageUrl']?>" alt="<?=esc_attr($post['imageAltText'])?>" height="<?=$post['imageHeight']?>" width="<?=$post['imageWidth']?>" src=""></figure>
    </a>
    <div>
        <span class="news__category">
            <span class="categoryAuthor">
                 <?php if (mb_strtoupper($catName)=='KOLUMNE' ) : ?>
                     <a  title="<?=$authorName?>" href="<?=$authorUrl?>"><?=$authorName?></a>
                 <?php elseif(mb_strtoupper($catName) === 'HPG'):?>
                     <span style="color:red;font-weight:bold;"><?=mb_strtoupper($catName)?></span>
                 <?php else : ?>
                     <a title="<?=$catName?>" href="<?=$catUrl?>"><?=$catName?></a>
                 <?php endif;?>
            </span>
            <span>
                 <span class="postDate"><i class="fa fa-clock"></i> <?= $timeAgo[0]?> <?= $timeAgo[1] ?></span>
                 <i style="color:gray; margin-left:10px; font-size:14px;" class="fas fa-comment"></i>
                <span style="color:gray; font-weight:bold;" class="cp-list-comments-num" data-story-id="<?= $article->getPostId() ?? '' ?>"></span>
            </span>
        </span>

        <h2><a title="<?=esc_attr($title)?>" href="<?=$permalink?>"><?=$title?></a></h2>
        <p><?= parseHeading($blocks)?></p>
    </div>
</article>