<?php
// Get post meta
$postMeta = get_post_meta(get_the_ID());
// Get thumbnail ids from the post meta
$thumbnailIdsString = $postMeta['di_multi_thumb_thumbs'][0];
// Make an array from the comma separated list of thumbnail ids
//$thumbnailIdsArray = explode(',', $thumbnailIdsString);
$idsArray =explode(',', $thumbnailIdsString);

if (!isAmp()){
    $ligthboxSize = '1536x1536';
    if ( wp_is_mobile() ) {
        $ligthboxSize = 'large';
    }
	echo do_shortcode('[gallery ids="'. $thumbnailIdsString .'" lightbox_max_size="'.$ligthboxSize.'"]');
} else {
	echo gallery_shortcode(['ids' => $idsArray]);
}

