<?php
if(!wp_is_mobile() && is_front_page()) {
    $blicNaslovnaPage = get_page_by_title('blic naslovna');
    $pageLink = get_permalink($blicNaslovnaPage->ID);
    global $wpdb;
    $query = "SELECT * FROM wp_options WHERE option_name = 'blicNaslovnaImg'";
    $imgId = (int)$wpdb->get_results($query)[0]->option_value;

    $imgSrc = cropBlicImage($imgId);

    $image = '<a href="%s" title="blic naslovna"> <img title="blic naslovna" alt="blic naslovna" src="%s" width="160" height="213"></a>';
    $html = sprintf('<div class="euroblic">' . $image .
        '<span><strong>Euroblic</strong> naslovna</span>
                    </div>', $pageLink, $imgSrc);
    echo $html;
}

