<?php
// classic content images
$id    = str_replace('attachment_','', $data['id']);
$align = $data['align'];
$width = $data['width'];
$imageCaption = wp_get_attachment_caption($id);
$imageLegenda = get_post_meta($id,'legenda',true);

if (wp_is_mobile()) {
    $imageUrl = wp_get_attachment_image_url($id, 'large');
    $imageWidth = '374';
    $imageHeight = '250';
} else {
    $imageUrl = wp_get_attachment_image_url($id, 'single');
    $imageWidth = '872';
    $imageHeight = '610';
}
?>
<figure>
        <img src="<?= $imageUrl ?>" alt="<?= esc_attr($imageCaption) ?>" width="<?= $imageWidth ?>" height="<?= $imageHeight ?>"/>
        <figcaption><?= $imageCaption ?></figcaption>
    <?php if (strlen($imageLegenda)) : ?>
        <figcaption><?= $imageLegenda ?></figcaption>
    <?php endif; ?>
</figure>