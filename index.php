<?php
get_header();

//Check for mobile & AMP
global $mobile;
$mobile = wp_is_mobile();

if ( have_posts() ):
    while ( have_posts() ) : the_post(); ?>
        <div id="post-<?php the_ID(); ?>" <?php post_class( 'post-wrapper' ); ?>>
            <?php get_the_ID(); ?>
            <?php
            the_content();
            wp_link_pages( array(
                'before' => '<div class="page-links">',
                'after'  => '</div>',
            ) );
            ?>
        </div>
    <?php
    endwhile;
else:
    echo '<p>' . _e( 'Žao nam je, ni jedna stranica se ne podudara sa Vašim kriterijumima', 'gfShopTheme' ) . '</p>';
endif;
get_footer(); ?>